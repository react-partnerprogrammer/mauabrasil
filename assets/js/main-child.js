(function ($, root, undefined) {
	$(function () {

    'use strict';

    // menu modal control
    $('li.check a').click(function (e) { 
      e.preventDefault();
      $('#checkAvailability').modal('show');
    });
    
    // menu control
		var app = function () {
      var body = undefined;
      var menu = undefined;
      var menuItems = undefined;
      var init = function init() {
        body = document.querySelector('body');
        menu = document.querySelector('.menu-icon');
        menuItems = document.querySelectorAll('.nav__list-item');
        applyListeners();
      };
      var applyListeners = function applyListeners() {
        menu.addEventListener('click', function () {
          return toggleClass(body, 'nav-active');
        });
      };
      var toggleClass = function toggleClass(element, stringClass) {
        if (element.classList.contains(stringClass)) element.classList.remove(stringClass);else element.classList.add(stringClass);
      };
      init();
    }();

    // custom dots
    function setBoundries(slick, state) {
      if (state === 'default') {
        slick.find('ul.slick-dots li').eq(4).addClass('n-small-1');
      }
    }
    var slickSlider = $('.cottages--slider');
    var maxDots = 5;
    var transformXIntervalNext = -18;
    var transformXIntervalPrev = 18;

    slickSlider.on('init', function (event, slick) {
      $(this).find('ul.slick-dots').wrap("<div class='slick-dots-container'></div>");
      $(this).find('ul.slick-dots li').each(function (index) {
        $(this).addClass('dot-index-' + index);
      });
      $(this).find('ul.slick-dots').css('transform', 'translateX(0)');
      setBoundries($(this),'default');
    });
  
    var transformCount = 0;
    slickSlider.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      var totalCount = $(this).find('.slick-dots li').length;
      if (totalCount > maxDots) {
        if (nextSlide > currentSlide) {
          if ($(this).find('ul.slick-dots li.dot-index-' + nextSlide).hasClass('n-small-1')) {
            if (!$(this).find('ul.slick-dots li:last-child').hasClass('n-small-1')) {
              transformCount = transformCount + transformXIntervalNext;
              $(this).find('ul.slick-dots li.dot-index-' + nextSlide).removeClass('n-small-1');
              var nextSlidePlusOne = nextSlide + 1;
              $(this).find('ul.slick-dots li.dot-index-' + nextSlidePlusOne).addClass('n-small-1');
              $(this).find('ul.slick-dots').css('transform', 'translateX(' + transformCount + 'px)');
              var pPointer = nextSlide - 3;
              var pPointerMinusOne = pPointer - 1;
              $(this).find('ul.slick-dots li').eq(pPointerMinusOne).removeClass('p-small-1');
              $(this).find('ul.slick-dots li').eq(pPointer).addClass('p-small-1');
            }
          }
        }
        else {
          if ($(this).find('ul.slick-dots li.dot-index-' + nextSlide).hasClass('p-small-1')) {
            if (!$(this).find('ul.slick-dots li:first-child').hasClass('p-small-1')) {
              transformCount = transformCount + transformXIntervalPrev;
              $(this).find('ul.slick-dots li.dot-index-' + nextSlide).removeClass('p-small-1');
              var nextSlidePlusOne = nextSlide - 1;
              $(this).find('ul.slick-dots li.dot-index-' + nextSlidePlusOne).addClass('p-small-1');
              $(this).find('ul.slick-dots').css('transform', 'translateX(' + transformCount + 'px)');
              var nPointer = currentSlide + 3;
              var nPointerMinusOne = nPointer - 1;
              $(this).find('ul.slick-dots li').eq(nPointer).removeClass('n-small-1');
              $(this).find('ul.slick-dots li').eq(nPointerMinusOne).addClass('n-small-1');
            }
          }
        }
      }
    });

    // cottages slider
    $('.cottages--slider').slick({
			dots: true,
			arrows: false,
			infinite: false,
			speed: 500,
			slidesToShow: 4,
      slidesToScroll: 1,
      focusOnSelect: true,
      rows: 0,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
            slidesToScroll: 1,
            focusOnSelect: true
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 2,
            slidesToScroll: 1,
            focusOnSelect: true
					}
				},
				{
					breakpoint: 768,
					settings: {
						slidesToShow: 1,
            slidesToScroll: 1,
            focusOnSelect: true
					}
				}
			]
    });
    
    // dishes slider
    $('.dishes--slider').slick({
			dots: true,
			arrows: false,
			infinite: false,
			speed: 500,
      rows: 2,
      slidesPerRow: 3,
      responsive: [
				{
					breakpoint: 768,
					settings: {
						rows: 2,
            slidesPerRow: 1,
					}
				}
			]
    });

    // dishes slider
    $('.testimonials--slider, .gallery--slider').slick({
			dots: true,
	    arrows: false,
			infinite: false,
			fade: false,
	    speed: 500,
	    slidesToShow: 1,
	    slidesToScroll: 1,
	    autoplay: true,
	    autoplaySpeed: 8000
    });

    // effects control
    var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});
    // new ScrollMagic.Scene({triggerElement: ".contact"})
    //   .setTween(".contact--bg", {y: "-10%", ease: Linear.easeNone})
    //   .addTo(controller);

    new ScrollMagic.Scene({triggerElement: ".main-header"})
    .setTween(".main-header--bg", {y: "-10%", ease: Linear.easeNone})
    // .addIndicators()
		.addTo(controller);
		
		$.each($('.img-parallax'), function (index, el) { 
			 
			 new ScrollMagic.Scene({triggerElement: el})
			 .setTween(el, {y: "-15%", ease: Linear.easeNone})
			//  .addIndicators()
			 .addTo(controller);
		});
    
	});
})(jQuery, this);