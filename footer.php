<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after.  Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.1
 */
?>

	<footer class="main-footer" role="contentinfo">
    <h1 class="sr-only">Rodapé</h1>
		<div class="container">
      <div class="row">
        <div class="col-lg-5 media align-items-center flex-column flex-md-row">
          <img src="<?php echo get_field('logo_footer', 'option'); ?>" class="img-fluid mr-md-5 mb-3 mb-xl-0" />

          <div class="media-body text-center text-md-left">
            <h2>Mauá Brasil</h2>
            <p><?php the_field('company_footer', 'option'); ?></p>
          </div>
        </div>

        <div class="col-md-3 col-lg-2 ml-auto mx-lg-auto text-center text-md-left mb-4 mb-md-0">
          <h2>Mapa do Site</h2>
          <?php
            wp_nav_menu([
              'menu'            => 'rodape',
              'container'       => 'div',
              'container_class' => '',
              'menu_id'         => false,
              'menu_class'      => 'navbar-nav',
              'depth'           => 2,
              'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
              'walker'          => new wp_bootstrap_navwalker()
            ]);
          ?>
        </div>
        <div class="col-md-6 col-lg-4 text-center text-md-left mb-4 mb-md-0">
          <h2>Contato</h2>
          <ul class="list-infos">
            <li><p><i class="fas fa-map-marker-alt mr-3 mt-2"></i> <?php the_field('address_info', 'option') ;?></p></li>
            <li>
              <p>
                <i class="fas fa-phone-volume mr-3 mt-2"></i>
                <a class='mask-phone' title='Ligue para nós!' href='tel:<?php $phone = get_field('phone_info', 'option');  echo $phone; ?>'><?php echo $phone; ?></a> 
                <?php if (get_field('phone_sec_info', 'option')) { ?>/
                  <a class='mask-phone' title='Ligue para nós!' href='tel:<?php $phone_sec = get_field('phone_sec_info', 'option');  echo $phone_sec; ?>'><?php echo $phone_sec; ?></a>
                <?php } ?>
              </p>
            </li>
            <li><p><i class="fab fa-whatsapp mr-3 mt-2"></i><a class='mask-phone' title='Ligue para nós!' href='https://api.whatsapp.com/send?phone=<?php $whats = get_field('cel_info', 'option'); echo $whats; ?>'><?php echo $whats; ?></a></p></li>

            <li><?php get_template_part('partials/_social-networks'); ?></li>
          </ul>
        </div>
      </div>
		</div>
  </footer>

  <div class="modal fade modal-availability" id="checkAvailability" tabindex="-1" role="dialog" aria-labelledby="checkAvailabilityLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h5 class="s-title w-100" id="checkAvailabilityLabel">Reservas</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <?php echo do_shortcode('[contact-form-7 id="223" title="Reservas"]'); ?>
        </div>
      </div>
    </div>
  </div>
  
<?php wp_footer(); ?>
</body>
</html>