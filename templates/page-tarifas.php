<?php
  /* Template name: Tarifas */
  get_header();
  get_template_part('partials/_wrap-start');
?>
  <div class="p-rates pt-control mb-5">
    
    <?php if ( have_rows('rates') ) : ?>
      <div class="p-rates--content container">
        <div class="row">
        <?php 
          while( have_rows('rates') ) : the_row(); 
          $rates = get_sub_field('names_rates');          
          if ($rates) {
            $chales = [];
            foreach ($rates as $rate) {
              $chales[] = $rate->post_title;
            }
          }
        ?>
          <div class="col-12 mb-5">
            <div class="row">
              <div class="col-12 mb-3">
                <div class="p-rates--box">
                  <p><?php if ($chales) echo implode(', ', $chales); ?></p>
                </div>
              </div>
              <div class="col-md-3">
                <div class="p-rates--box">
                  <p><?php the_sub_field('values_rates'); ?></p>
                </div>
              </div>
              <div class="col-md-3">
                <div class="p-rates--box">
                  <p><?php the_sub_field('values_sec_rates'); ?></p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="p-rates--box">
                  <p><?php the_sub_field('detais_rates'); ?></p>
                </div>
              </div>
            </div>
          </div>
        <?php endwhile; ?>
        </div>
      </div>
    <?php endif; ?>

    <?php if ( get_field('general_rates') || get_field('cancel_rates') ) : ?>
      <div class="p-rates--footer mb-5">
        <div class="container">
          <h2 class="s-title">Tarifas</h2>
          <div class="row">
            <?php if ( get_field('general_rates') ) : ?>
              <div class="col-12 mb-5">
                <h3 class="s-title">Observações gerais:</h3>
                <?php echo get_field('general_rates'); ?>
              </div>
            <?php endif; ?>

            <?php if ( get_field('cancel_rates') ) : ?>
              <div class="col-12">
                <h3 class="s-title">Política de Cancelamento:</h3>
                <?php echo get_field('cancel_rates'); ?>
              </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>
  </div>
    
<?php
  get_template_part('partials/_wrap-end');
  get_footer();
  