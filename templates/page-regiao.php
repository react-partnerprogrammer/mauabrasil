<?php
  /* Template name: Região */
  get_header();
  get_template_part('partials/_wrap-start');
?>
  <div class="p-region has-bg">
    <div class="container">
      <div class="row">
        <div class="col-md-4 mx-auto">
          <?php if ( get_field('content_region') ) : ?>
            <?php echo get_field('content_region'); ?>
          <?php endif; ?>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row position-relative">
        <div class="col-md-6">
          <h3 class="s-title mb-5"><?php the_field('title_restaurant'); ?></h3>
          <?php the_field('desc_restaurant'); ?>
        </div>

        <div class="full-right">
          <?php if ( get_field('thumb_restaurant') ) : ?>
            <img src="<?php the_field('thumb_restaurant'); ?>" alt="<?php the_field('title_restaurant'); ?>" class="img-fluid">
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
<?php
  get_template_part('partials/_wrap-end');
  get_footer();