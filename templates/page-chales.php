<?php
  /* Template name: Chalés */
  get_header();
  get_template_part('partials/_wrap-start');
  /*
?>
  <div class="p-cottage">
    <div class="p-cottage--hotspots">
      <?php
        $hotspots = get_field('hotspots');
        if ($hotspots) {
          echo '<img src="'.$hotspots[0]['bg_hotspot'].'" class="img-fluid">';
        }
        $i=1; foreach ($hotspots as $hotspot) {
          $marker = $hotspot['ico_hotspot'];
          $title  = $hotspot['title_hotspot'];
          $desc   = $hotspot['desc_hotspot'];
          $thumb  = $hotspot['thumb_hotspot'];
          $coords = explode( ',', $marker );
          $link   = $hotspot['link_hotspot'];
      ?>
        <button type="button" data-toggle="modal" data-target="#chale<?php echo $i; ?>" class="marker" style="left:<?php echo $coords[0]; ?>;top:<?php echo $coords[1]; ?>;"><i></i><?php echo $i; ?></button>        
      <?php $i++; } ?>
    </div>

    <?php
    */
      $loop = new WP_Query( ['post_type' => 'cottage', 'posts_per_page' => -1 ] );
      if ( $loop->have_posts() ) :
    ?>
    <div class="p-cottage--content">
      <div class="container">
        <div class="row">
          <?php $i=1; while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <div class="col-md-7 <?php echo !($i %2) ? 'ml-auto' : 'mr-auto' ; ?>">
              <article class="box-cottage">
                <figure class="mb-3 mb-lg-5">
                  <a href="<?php the_permalink(); ?>" title="Conheça mais: <?php the_title(); ?>" aria-hidden="true" tabindex="-1">
                    <img src="<?php echo get_field('into_thumb_scottage')['sizes']['post-medium']; ?>" class="img-fluid has-mask">
                  </a>
                </figure>

                <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
              </article>
            </div>
          <?php $i++; endwhile; ?>
        </div>
      </div>
    </div>
    <?php
      wp_reset_postdata();
      endif;
    ?>

    <div class="container p-cottage--footer">
      <div class="row position-relative">
        <div class="col-md-7">
          <h3 class="s-title mb-3 mb-lg-5"><?php the_field('title_restaurant'); ?></h3>
          <?php the_field('desc_restaurant'); ?>
        </div>

        <div class="full-right">
          <?php if ( get_field('thumb_restaurant') ) : ?>
            <img src="<?php the_field('thumb_restaurant'); ?>" alt="<?php the_field('title_restaurant'); ?>" class="img-fluid">
          <?php endif; ?>
        </div>
      </div>
    </div>

  </div>
    
<?php
  get_template_part('partials/_wrap-end');
/*
  echo '<div class="modals">';
    $i=1; foreach ($hotspots as $hotspot) {
      $marker = $hotspot['ico_hotspot'];
      $title  = $hotspot['title_hotspot'];
      $desc   = $hotspot['desc_hotspot'];
      $thumb  = $hotspot['thumb_hotspot'];
      $coords = explode( ',', $marker );
      $link   = $hotspot['link_hotspot'];
  ?>
  <div class="modal modal-bs fade" id="chale<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="chale<?php echo $i; ?>Title" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <img src="<?php echo $thumb ?>" class="thumb" />

      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <h3 class="m-0"><?php echo $title; ?></h3>
          <p class="m-0"><?php echo $desc; ?></p>
          <a href="<?php echo $link['url']; ?>"><?php echo $link['title']; ?></a>
        </div>
      </div>
    </div>
  </div>
<?php  $i++; }
echo '</div>'; // .modals
*/
get_footer();