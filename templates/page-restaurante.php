<?php
  /* Template name: Restaurante */
  get_header();
  get_template_part('partials/_wrap-start');
?>
  <div class="dishes has-bg">
    <div class="container">
      <?php if ( have_rows('dishe') ) : ?>
        <div class="dishes--slider">  
          <?php while( have_rows('dishe') ) : the_row(); ?>
        
            <div>
              <article class="box-dishe">
                <figure>
                  <a href="<?php echo get_sub_field('thumb_dishe')['url']; ?>" data-fancybox="gallery">
                    <img src="<?php echo get_sub_field('thumb_dishe')['sizes']['restaurant-thumb']; ?>" class="img-fluid">
                  </a>
                </figure>                
                <?php echo '<h2 class="text-center m-0">' . get_sub_field('title_dishe') .'</h2>'; ?>                
              </article>
            </div>
        
          <?php endwhile; ?>
        </div>
      <?php endif; ?>
    </div>

    <div class="container">
      <div class="row position-relative">
        <div class="col-lg-6">
          <h3 class="s-title mb-5"><?php the_field('title_restaurant'); ?></h3>
          <?php the_field('desc_restaurant'); ?>
          <br><br>
        </div>

        <div class="full-right">
          <?php if ( get_field('thumb_restaurant') ) : ?>
            <img src="<?php the_field('thumb_restaurant'); ?>" alt="<?php the_field('title_restaurant'); ?>" class="img-fluid">
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
<?php
  get_template_part('partials/_wrap-end');
  get_footer();