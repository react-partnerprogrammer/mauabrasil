<?php
  /* Template name: Experiência */
  get_header();
  get_template_part('partials/_wrap-start');
?>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-11 mx-auto">
        <?php 
          if ( have_rows('content_experience') ) :
          $i=1; while( have_rows('content_experience') ) : the_row(); 
        ?>
          <article class="box-experience">
            <div class="row align-items-center <?php echo !($i % 2) ? 'flex-row-reverse' : 'flex-row'; ?>">
              <div class="col-md-6">
                <h2 class="s-title"><?php the_sub_field('title_exp'); ?></h2>
                <p><?php the_sub_field('desc_exp'); ?></p>
              </div>
              <div class="col-md-6">
                <?php 
                  $images = get_sub_field('gallery_exp');
                  if( $images ): 
                ?>
                  <div class="testimonials--slider">
                    <?php foreach( $images as $image ): ?>
                      <div>
                        <img src="<?php echo $image['sizes']['experience-thumb']; ?>" class="img-fluid w-100 mt-5 mb-4" />
                      </div>
                    <?php endforeach; ?>
                  </div>
                <?php endif; ?>
              </div>
            </div>
          </article>  
        <?php 
          $i++; endwhile;
          endif; 
        ?>
      </div>
    </div>
  </div>

<?php
  get_template_part('partials/_wrap-end');
  get_footer();