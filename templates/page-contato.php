<?php
  /* Template name: Contato */
  get_header();
  get_template_part('partials/_wrap-start');
?>
  <div class="p-contato mb-5">
    <?php
      $location = get_field('location_info', 'option');
      if( !empty($location) ):
    ?>
      <div class="acf-map">
        <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
      </div>
    <?php endif; ?>
  </div>
    
<?php
  get_template_part('partials/_wrap-end');
  get_footer();