<?php
// register custom post type
require_once( get_stylesheet_directory() .'/includes/cpt-cottage.php' );

// custom image sizes
add_action('init', 'child_custom_image_sizes');
function child_custom_image_sizes() {
  add_image_size('header', 1920, 1280, true);
  add_image_size('post-medium', 800, 480, true);
  add_image_size('post-thumb', 300, 250, true);
  add_image_size('chale-thumb', 340, 480, true);
  add_image_size('experience-thumb', 580, 350, true);
  add_image_size('restaurant-thumb', 435, 260, true);
}

// load css of child theme
add_action( 'wp_enqueue_scripts', 'child_enqueue_styles' );
function child_enqueue_styles() {
  wp_enqueue_style( 'mauabrasil-child', get_stylesheet_directory_uri() . '/assets/css/main-child.css' );
}

add_action( 'wp_print_scripts', 'child_enqueue_scripts' );
function child_enqueue_scripts() {
  $js_full = get_stylesheet_directory_uri() . '/assets/js/';
  $js = get_stylesheet_directory_uri() . '/assets/js/min/';
  
  if (!is_admin()) {
    wp_enqueue_script('tweenmax',          '//cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/TweenMax.min.js', ['jquery']);
    wp_enqueue_script('scrollmagic',       '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/ScrollMagic.min.js', ['jquery']);
    wp_enqueue_script('animation',         '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/animation.gsap.min.js', ['jquery']);
    wp_enqueue_script('scrollmagic-debug', '//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.6/plugins/debug.addIndicators.min.js', ['scrollmagic']);

    wp_enqueue_script('main-child', $js_full . 'main-child.js', ['jquery']);
  }
}

function open_tag($tag, $attr) {
  $attr_string = generate_attr_string($attr);
  return "<" . $tag . " " . $attr_string . ">"; 
}

function close_tag($tag) {
  return "</" . $tag . ">";
}

// _p
function _p($tag = 'p', $content, $args = []) {
    
  // If you pass a string as $arg argument, this will be setup as class.
  if ($args && is_string($args)) {
    $args = ['class' => $args];
  }

  $defaults = array (      
    'class' => '',      
    'before' => '',
    'attr' => [],
    'after' => '',
    'echo' => true
 );

  // Parse incoming $args into an array and merge it with $defaults
  $args = wp_parse_args( $args, $defaults );
  if ($args['class']) { $args['attr']['class'] = $args['class']; }    
  
  
    // Add special filters  
    $check = explode(':', $tag);
    if (count($check) > 1 ) {
      $tag = $check[0];
      $special_tag = $check[1];

      // Update the url to href atrribute
      if ($special_tag == 'acf-link') {

        $args['attr']['href'] = $args['attr']['url'];
        unset($args['attr']['url']);
      
      // Check if the IMAGE has Link and add 
      } elseif ($special_tag == 'acf-image') {

        $image_ID = isset($content['ID']) ? $content['ID'] : false;
        $image_data = wp_get_attachment($image_ID);
        $image_acf_link = $image_ID && get_field('link', $image_ID) ? get_field('link', $image_ID) : false;          
        
        // Check if has lightbox
        $lightbox = get_field('abrir_em_lightbox', $image_ID );

        // Setup extra datas for the image
        $args['attr']['src'] = $content['url'];
        $args['attr']['alt'] = $image_data['alt'];

        // Clear content
        $content = ' ';
        
        if ($image_acf_link) {
          $title = $image_acf_link['title'] ? $image_acf_link['title'] : '';
          $target = $image_acf_link['target'] ? $image_acf_link['target'] : '_self';
          
          $attr =  $lightbox ? ' data-fancybox ' : '';
          
          $args['before'] = '<a title="'.$title.'" href="'.$image_acf_link['url'].'" '.$attr.' target="'. $target .'">' . $args['before'];
          $args['after'] = $args['after'] . '</a>';          
        } // if ($image_acf_link) 

      } // elseif ($special_tag == 'acf-image')         

    } //if (count($check) > 1 ) {
      
  // Check if has content
  if ($content) {
    $output  = $args['before'];
    
      $output .= open_tag($tag, $args['attr']) . $content . close_tag($tag);

    $output .= $args['after'];

    if (isset($args['echo']) && $args['echo'] === false) 
      return $output;
    
    echo $output;

  }
}

/* Posts Relacionados */
/* ----------------------------------------- */
function pp_related($args = []) { 
  global $post;
  // extract($args);
  // echo '<pre>'. print_r($args, 1) . '</pre>';
    $defaults = array (      
      'titleClass' => '',
      'descricao' => '',
    );

  // Parse incoming $args into an array and merge it with $defaults
  $args = wp_parse_args( $args, $defaults );

    // Define alguns argumentos baseado no post type object
    $postTypeObj = get_post_type_object($post->post_type);    
    $taxonomies = isset($args['taxonomies']) ? $args['taxonomies'] : $postTypeObj->taxonomies;
    $taxonomies = $taxonomies ? $taxonomies : ['category'];

    // echo '<pre>'. print_r($postTypeObj, 1) . '</pre>';
    
    $defaultargsQuery = $argsQuery = array(       
      'post__not_in' => array($post->ID), 
      'post_type' => $post->post_type,
      'posts_per_page' => (isset($args['posts_per_page']) ? $args['posts_per_page'] : 3), 
    );

    // Verifica se existem termos relacionadas ao post
    $terms = wp_get_post_terms($post->ID, $taxonomies, ['fields' => 'ids']);
    // Se existir, adiciona a query
    $argsQuery['tax_query'] = [
      [
        'taxonomy' => $taxonomies[0],
        'field' => 'term_id',
        'terms' => $terms
      ]
    ];

    $relatedPostsQuery = new WP_Query($argsQuery);
    // Se não existir nenhum relacionado, pega qualquer outro post
    if (!$relatedPostsQuery->have_posts()) {
      $relatedPostsQuery = new WP_Query($defaultargsQuery);
    } 

    if( $relatedPostsQuery->have_posts() ) {
      echo  '<div id="post-relacionados" class="my-5 related '.$args['class'].'"><div class="container"><div class="row">',
              '<div class="col-md-10 mx-auto">',
                '<div class="row"><div class="col-12"><h4 class="s-title '.$args['titleClass'].'">'.(isset($args['title']) ? $args['title'] : __('Veja outros', 'react')).'</h4></div></div>',
                  '<div class="row">';
                
                    _p('p', $args['descricao'], [ 'class' => 'rubik h4 font-weight-normal mb-3 descricao', 'echo' => 'false']);
                      while ( $relatedPostsQuery->have_posts() ) : $relatedPostsQuery->the_post();
                        get_template_part('contents/_loop-related');
                      endwhile;

      echo        '</div>',
            '</div></div></div>';
    } // endif

  wp_reset_query(); 

}
/* ----------------------------------------- posts relacionados */

function generate_attr_string($attr) {
  // echo '<pre>'.print_r($attr). '</pre>';
  // die();
  $attr_string = null;
  if (!empty($attr)) {
    foreach ($attr as $key => $value) {
      // If we have attributes, loop through the key/value pairs passed in
      //and return result HTML as a string

      // Don't put a space after the last value
      if ($value == end($attr)) {
        $attr_string .= $key . "=" . '"' . $value . '"';
      } else {
        $attr_string .= $key . "=" . '"' . $value . '" ';
      }
    }
  }       
  return $attr_string;      
}

function my_list_categories($taxonomy = 'category') {
  global $post;
  $return = '';
  // echo '<pre>'.print_r(get_the_category($post->ID),1). '</pre>';
  // die();
  if (get_the_terms($post->ID, $taxonomy)) {
    $categories = [];
    foreach (get_the_terms($post->ID, $taxonomy) as $category) {
      $categories[] = $category->name;
    }
    $return .= implode(', ', $categories);
  }
  
  return $return;
}