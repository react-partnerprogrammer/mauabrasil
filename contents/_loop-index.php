<div class="container my-5">
	<div class="row">

		<div class="col-lg-9">
      <?php 
        if ( have_posts() ): 
          while( have_posts() ): the_post();
          
            get_template_part('contents/_loop');

          endwhile; 
          if (function_exists('wp_pagenavi')) { wp_pagenavi(); };
        endif; 
      ?>
		</div>

		<?php get_sidebar(); ?>

	</div>
</div>