<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
	<article <?php post_class( 'singular-cottage pb-5' ); ?>>
    <?php if ( have_rows('days_values_scottage') ) : ?>
      <header class="singular-cottage--header">
        <div class="container">
          <div class="row justify-content-center">
            <?php while( have_rows('days_values_scottage') ) : the_row(); ?>
              <div class="col-md-4 text-center mb-5">
                <h4 class="s-title small w-50 mx-auto mb-3 mb-lg-5 text-md-center"><?php the_sub_field('day_scottage'); ?></h4>
                <?php if( get_sub_field('value_scottage') ) : ?>
                  <p>R$ <span><?php the_sub_field('value_scottage'); ?></span></p>
                <?php endif; ?>
              </div>
            <?php endwhile; ?>

            <?php if ( get_field('obs_scottage') ) : ?>
              <div class="col-12">
                <p class="text-center obs"><?php echo get_field('obs_scottage'); ?></p>
              </div>
            <?php endif; ?>
            
          </div>
        </div>
      </header>
    <?php endif; ?>
    
		<div class="singular-cottage--content">

      <div id="disponibilidade" class="pt-5">
        <?php get_template_part('partials/_form-availability'); ?>
      </div>

      <?php if ( get_field('desc_scottage') || get_field('gallery_comfort') ) : ?>
        <div class="comfort">
          <div class="container">
            <div class="row">
              <div class="col-md-4 mx-auto text-center">
                <h2 class="s-title text-center m-0">Comodidades</h2>

                <?php if ( get_field('desc_scottage') ) : ?>
                  <p class="mb-5"><?php echo get_field('desc_scottage'); ?></p>
                <?php endif; ?>

                <?php 
                  $images = get_field('gallery_comfort');
                  if( $images ): 
                ?>
                  <ul class="list-unstyled d-flex flex-wrap flex-row align-items-center justify-content-center">
                    <?php foreach( $images as $image ): ?>
                      <li class="mx-3">
                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['title']; ?>">
                      </li>
                    <?php endforeach; ?>
                  </ul>
                <?php endif; ?>

                <a href="#disponibilidade" class="check smoothscroll">Checar disponibilidade</a>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>

      <?php if ( have_rows('testimonials_scottage', 137) ) : ?>
        <div class="testimonials detail-c detail-c__bottom">
          <div class="container">
            <h2 class="s-title text-center">Depoimentos</h2>
            <div class="row">
              <div class="col-md-6 mx-auto">
                <div class="testimonials--slider">
                  <?php while( have_rows('testimonials_scottage', 137) ) : the_row(); ?>
          
                    <div>
                      <?php if ( get_sub_field('desc_testimonial') ) : ?>
                        <p><?php the_sub_field('desc_testimonial'); ?></p>
                      <?php endif; ?>
                      
                      <p class="text-center name"><?php the_sub_field('name_testimonial'); ?></p>
                    </div>
                
                  <?php endwhile; ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
            
    </div>

    <?php
      $thumb   = get_field('bg_hotspot');
      $marker  = get_field('ico_hotspot');
      $title   = get_field('title_hotspot');
      $coords  = explode( ',', $marker );
        
      if ($thumb) {
        echo  '<footer class="singular-cottage--footer">',
                '<figure><img src="'.$thumb.'" class="img-fluid"></figure>',
                '<span class="marker" data-title="'.$title.'" style="left:'.$coords[0].';top:'.$coords[1].';"></span>',
              '</footer>';
      }
    ?>
	</article>
<?php endwhile; ?>