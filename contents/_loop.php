<article class="mb-5 mb-md-4 content-post">
  <a href="<?php the_permalink(); ?>" class="media flex-row">
    <?php 
      if (has_post_thumbnail()) {
        the_post_thumbnail('post-thumb', [ 
          'class' => 'd-flex align-self-start ml-4 ml-md-0 mb-md-3 mr-md-4 img-fluid order-2 order-md-1',
          'alt' => get_the_title()
        ] );
      }
    ?>
    <div class="media-body order-1 order-md-2">
      <?php 
        the_title( '<h2 class="h4">', '</h2>' );

        echo '<span class="excerpt d-none d-md-block">';
          the_excerpt();
        echo '</span>';
      ?>
      <p class="h6"><?php echo __('Por', 'react') . ' '; the_author(); ?> - <?php the_time( get_option( 'date_format' ) ); ?></p>
    </div>
  </a>
</article>