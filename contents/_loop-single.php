<article id="post-<?php the_ID(); ?>" <?php post_class('container singular-post'); ?> >
    
  <div class="conteudo col-lg-10 col-xl-8 mx-auto">
    <?php 
      global $post;
      $obj = get_post_type_object( $post->post_type );	
          
      the_content();

      echo '<div class="my-6">'.do_shortcode('[easy-social-share]').'</div>';
      
      get_template_part('partials/_comments');
      
    ?>
  </div>

  <?php 
    _p('a', __(' < Voltar', 'react'), [ 
      'attr' => [ 
        'href' => get_permalink(get_option( 'page_for_posts' )),
        'class' => 'btn btn-gold rubik mx-auto text-white',
      ],
    ]);

    echo '<hr class="my-6" />';

    pp_related([
      'class' => 'secao--blog mb-8 mt-4 related--' . $post->post_type,
      'title' => __('Veja também', 'react'),
      'loop' => 'related-' . $post->post_type,
      'posts_per_page' => 3
    ]);
  ?>

</article>