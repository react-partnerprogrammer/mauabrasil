<?php

// custom image sizes
add_action('init', 'child_custom_image_sizes');
function child_custom_image_sizes() {
  add_image_size('slide', 1920, 1105, true);
}