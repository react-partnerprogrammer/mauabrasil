<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />

		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<link href="<?php images_url('favicon.ico'); ?>" rel="shortcut icon" type="image/x-icon" />

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- google maps -->
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1P-H_fyEh6IaGS_mdIAPnMUIiQhKON2s"></script>
    <?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
    
    <header class="main-header <?php echo (get_field('video_header')) ? 'no-mask' : '' ; ?>">
      <?php 
        if (is_front_page() && !get_field('video_header')) {
          echo '<h1 class="s-title mb-5">'.esc_attr( get_bloginfo( 'name', 'display' ) ).'</h1>';

        }  elseif (is_home() || is_singular('post') && !get_field('video_header')) {
          echo '<h1 class="s-title">Blog</h1>';
          
        } elseif (!is_page_template('templates/page-contato.php') && !get_field('video_header')) {
          the_title( '<h1 class="s-title">', '</h1>' );

        }
      ?>

      <?php if (is_page_template('templates/page-contato.php')) { ?>
        <div class="container container-contact">
          <div class="row">
            <div class="col-12">
              <?php the_title( '<h1 class="s-title mb-4 mb-md-5">', '</h1>' ); ?>
            </div>
            <div class="col-md-6 col-xl-4">
              <?php echo do_shortcode( '[contact-form-7 id="9" title="Contato"]' );  ?>
            </div>
            <div class="col-md-6 col-xl-5 pl-xl-5">
              <?php get_template_part('partials/_list-infos'); ?>
            </div>
          </div>
        </div>
      <?php } ?>

      <div class="nav-btn">
        <div class="menu-icon hover-target">
          <span class="menu-icon__line menu-icon__line-left"></span>
          <span class="menu-icon__line"></span>
          <span class="menu-icon__line menu-icon__line-right"></span>
        </div>
      </div>

      <nav class="nav">
        <?php
          wp_nav_menu([
            'theme_location'  => 'global',
            'container'       => 'div',
            'container_class' => 'nav--content',
            'menu_id'         => false,
            'menu_class'      => 'nav__list',
            'depth'           => 2,
            'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
            'walker'          => new wp_bootstrap_navwalker()
          ]);
        ?>
      </nav>
      
      <?php if ( get_field('image_video') == "video" ) : ?>
        <div class="videoContainer">
          <video autoplay muted playsinline="">
            <source src="<?php the_field('video_header'); ?>" type="video/mp4;">
            <source src="<?php the_field('video_webm_header'); ?>" type="video/webm;">
            <source src="<?php the_field('video_ovg_header'); ?>" type="video/ogg;">
          </video>

        </div>
      <?php else : ?>
        <?php 
          if (is_singular('cottage')) : 
            get_template_part( 'partials/_gallery' );
          else : 
        ?>
        
        <span 
        class="main-header--bg d-flex align-items-center justify-content-center text-center" 
        style="background-image: url(
          <?php 
            if (is_home() || is_singular('post')) {
              echo get_field('image_header', get_option('page_for_posts'))['sizes']['header']; 
            } else { 
              echo get_field('image_header')['sizes']['header']; 
            } 
          ?>);"></span>

        <?php endif; ?>
      <?php endif; ?>
    </header>
