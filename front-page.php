<?php
  get_header();

    get_template_part('partials/_wrap-start');
          
      get_template_part('partials/_cta');
      get_template_part('partials/_form-availability');
      get_template_part('partials/_cottages');
      get_template_part('partials/_about');
      get_template_part('partials/_nature');
      get_template_part('partials/_restaurant');
      get_template_part('partials/_region');
      get_template_part('partials/_contact');
  	
  	get_template_part('partials/_wrap-end');

  get_footer();