<ul class="list-infos">
  <li>
    <i class="fas fa-map-marker-alt mr-3 mt-2"></i>
    <p><?php the_field('address_info', 'option') ;?></p>
  </li>
  <li>
    <i class="fas fa-phone-volume mr-3"></i>
    <?php 
      $phone = get_field('phone_info', 'option'); 
      $phone_sec = get_field('phone_sec_info', 'option'); 
    ?>
    <p>
      <a class='mask-phone' title='Ligue para nós!' href='tel:<?php echo $phone; ?>'><?php echo $phone; ?></a> 
      <?php if ($phone_sec) { ?>/ <a class='mask-phone' title='Ligue para nós!' href='tel:<?php echo $phone_sec; ?>'><?php echo $phone_sec; ?></a><?php } ?>
    </p>
  </li>
  <li>
    <i class="fab fa-whatsapp mr-3 mt-2"></i>
    <a class='mask-phone' title='Ligue para nós!' href='https://api.whatsapp.com/send?phone=<?php $whats = get_field('cel_info', 'option'); echo $whats; ?>'><?php echo $whats; ?></a>
  </li>  
</ul>