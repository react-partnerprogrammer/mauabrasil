<div class="nature">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-md-6 col-lg-4 mx-auto d-flex flex-column order-2 order-md-1">
        <h1 class="s-title"><?php the_field('nature_title_section'); ?></h1>
        <?php if ( get_field('nature_desc_section') ) : ?>
          <p class="mb-3 mb-lg-5 "><?php echo get_field('nature_desc_section'); ?></p>
        <?php endif; ?>
        <?php if ( get_field('nature_btn_section') ) : ?>
          <a href="<?php echo get_field('nature_btn_section')['url']; ?>" class="btn btn--classic ml-auto d-none d-md-inline-block"><?php echo get_field('nature_btn_section')['title']; ?></a>
        <?php endif; ?>
      </div>

      <div class="col-md-6 col-lg-6 p-0 order-1 order-md-2">
        <?php if ( get_field('nature_image_section') ) : ?>
          <img src="<?php the_field('nature_image_section'); ?>" alt="<?php the_field('nature_title_section'); ?>" class="img-fluid img-parallax">
        <?php endif; ?>
      </div>

      <div class="col-12 d-flex d-md-none order-3 mb-5">
        <?php if ( get_field('nature_btn_section') ) : ?>
          <a href="<?php echo get_field('nature_btn_section')['url']; ?>" class="btn btn--classic mx-auto mt-3 "><?php echo get_field('nature_btn_section')['title']; ?></a>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>