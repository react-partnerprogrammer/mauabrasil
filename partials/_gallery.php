<?php 
  $images = get_field('gallery_scottage', get_the_ID());  
  if( $images ):
  // echo '<a href="#" data-toggle="modal" data-target="#checkAvailability" class="check check-float">Checar disponibilidade</a>';
  echo '<a href="#disponibilidade" class="smoothscroll check check-float">Checar disponibilidade</a>';
?>
  <div class="gallery--slider">
    <?php foreach( $images as $image ): ?>
      <div>
      <span 
        class="main-header--bg d-flex align-items-center justify-content-center text-center" 
        style="background-image: url(<?php echo $image['sizes']['header']; ?>);"></span>
      </div>
    <?php endforeach; ?>
  </ul>
<?php endif; ?>
