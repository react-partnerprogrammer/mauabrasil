<?php
  $loop = new WP_Query( ['post_type' => 'cottage', 'posts_per_page' => -1 ] );
  if ( $loop->have_posts() ) :
?>
<div class="cottages text-center">
  <h1 class="s-title text-center">Chalés</h1>
  <div class="container">
    <div class="row">
      <div class="col-md-11 mx-auto">
        <div class="cottages--slider">
          <?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
            <article class="box-cottage mb-lg-5">
              <figure class="mb-3 mb-lg-5">
                <a href="<?php the_permalink(); ?>" title="Conheça mais: <?php the_title(); ?>" aria-hidden="true" tabindex="-1" class="has-effect">
                  <img src="<?php echo get_field('home_thumb_scottage')['sizes']['chale-thumb']; ?>" class="img-fluid">
                </a>
              </figure>

              <?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
            </article>
          <?php endwhile; ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php
  wp_reset_postdata();
  endif;
?>
