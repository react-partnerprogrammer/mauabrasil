<div class="about">
  <h1 class="sr-only">A pousada</h1>

  <div class="container-fluid">
    <div class="row align-items-center">

      <div class="col-md-6 p-0 order-2 order-md-1">
        <?php if ( get_field('thumb_about') ) : ?>
          <img src="<?php the_field('thumb_about'); ?>" alt="A Pousada" class="img-fluid img-parallax">
        <?php endif; ?>
      </div>

      <div class="col-md-6 col-lg-4 mx-lg-auto d-flex flex-column order-1 order-md-2">
        <h2 class="s-title">A pousada</h2>
        <?php if ( get_field('desc_about') ) : ?>
          <p class="mb-3 mb-lg-5"><?php echo get_field('desc_about'); ?></p>
        <?php endif; ?>

        <?php if ( get_field('btn_about') ) : ?>
          <a href="<?php echo get_field('btn_about')['url']; ?>" class="btn btn--classic ml-md-auto d-none d-md-inline-block"><?php echo get_field('btn_about')['title']; ?></a>
        <?php endif; ?>
      </div>

      <div class="col-12 d-flex d-md-none order-3 mb-5">
        <?php if ( get_field('btn_about') ) : ?>
          <a href="<?php echo get_field('btn_about')['url']; ?>" class="btn btn--classic mx-auto mt-3"><?php echo get_field('btn_about')['title']; ?></a>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>