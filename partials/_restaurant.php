<div class="restaurant">
  <div class="container-fluid">
    <div class="row align-items-center">
      <div class="col-md-6 p-0">
        <?php if ( get_field('restaurant_image_section') ) : ?>
          <img src="<?php the_field('restaurant_image_section'); ?>" alt="<?php the_field('restaurant_title_section'); ?>" class="img-fluid img-parallax">
        <?php endif; ?>
      </div>

      <div class="col-md-6 col-lg-4 mx-auto d-flex flex-column mb-5 mb-md-0">
        <h1 class="s-title"><?php the_field('restaurant_title_section'); ?></h1>
        <?php if ( get_field('restaurant_desc_section') ) : ?>
          <p class="mb-5"><?php echo get_field('restaurant_desc_section'); ?></p>
        <?php endif; ?>
        <?php if ( get_field('restaurant_btn_section') ) : ?>
          <a href="<?php echo get_field('restaurant_btn_section')['url']; ?>" class="btn btn--classic mx-auto mr-md-0"><?php echo get_field('restaurant_btn_section')['title']; ?></a>
        <?php endif; ?>
      </div>
    </div>
  </div>
</div>