<?php if ( have_rows('buttons') ) : ?>
  <ul class="list-buttons mt-5 mb-3">
    <?php 
      while( have_rows('buttons') ) : the_row(); 
      $link = get_sub_field('button');
    ?>
      <li>
        <a href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" class="btn btn--classic">
          <?php echo $link['title']; ?>
        </a>
      </li>
    <?php endwhile; ?>
  </ul>
<?php endif; ?>
