<div class="contact">
  <h1 class="sr-only">Contato</h1>

  <div class="contact--form">
    <h2 class="s-title color-qui text-center">Contato</h2>
    <?php echo do_shortcode( '[contact-form-7 id="9" title="Contato"]' ); ?>
  </div>
  <span class="contact--bg d-flex align-items-center justify-content-center text-center" style="background-image: url(<?php the_field('contact_image_section'); ?>);"></span>
  
</div>